#!/bin/bash
fbturbo="xf86-video-fbturbo-git"
mesa="mesa-git"
fbturbofile="/etc/X11/xorg.conf.d/99-fbturbo.conf"
GLES_en=('qt5-es2-base qt5-es2-declarative qt5-es2-multimedia')
GLES_di=('qt5-base  qt5-declarative qt5-multimedia')
QT_file="/etc/environment"
echo "Deinstalling Fbturbo" 
sudo pacman -R ${fbturbo} --noconfirm
sudo rm -f ${fbturbofile}
echo "Installing newest Graphic Driver"
sudo pacman -S ${mesa} --noconfirm
echo "Installing OpenGLES enabled QT5 packages" 
sudo pacman -S ${GLES_en} --noconfirm
echo "QT_Software backend to Hardware" 
sudo sed -i 's/QT_QUICK_BACKEND=software/#QT_QUICK_BACKEND=software/g' ${QT_file}
echo "!!!!! ------------------------------------------------------------!!!!!"
echo "! Please enable Compositor on startup and change xrender to OpenGL2.0 !"
echo "!!!!! ------------------------------------------------------------!!!!!"
echo "Please reboot afterwards"