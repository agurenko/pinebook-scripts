#!/bin/bash
fbturbo="xf86-video-fbturbo-git"
mesa="mesa"
fbturbofile="/etc/X11/xorg.conf.d/99-fbturbo.conf"
GLES_en=('qt5-es2-base qt5-es2-declarative qt5-es2-multimedia')
GLES_di=('qt5-base  qt5-declarative qt5-multimedia')
QT_file="/etc/environment"
echo "Installing FBturbo" 
sudo pacman -S ${fbturbo} --noconfirm
echo "Restoring mesa"
sudo pacman -S ${mesa} --noconfirm
echo "Installing non_OpenGLES QT5 packages" 
sudo pacman -S ${GLES_di} --noconfirm
echo "QT_Software backend to Software" 
sudo sed -i 's/#QT_QUICK_BACKEND=software/QT_QUICK_BACKEND=software/g' ${QT_file}
echo "!!!!! ------------------------------------------------------------ !!!!!"
echo "! Please disable Compositor on startup and change OpenGL2.0 to xrender !"
echo "!!!!! ------------------------------------------------------------ !!!!!"
echo "Please reboot your Laptop"
